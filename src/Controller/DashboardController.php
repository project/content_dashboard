<?php

namespace Drupal\content_dashboard\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Dashboard Controller.
 */
class DashboardController extends ControllerBase {

  use StringTranslationTrait;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new DashboardController object.
   *
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(AccountInterface $current_user, EntityTypeManagerInterface $entity_type_manager) {
    $this->currentUser = $current_user;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function content() {
    return [
      '#theme' => 'content_dashboard',
      '#roles_defined' => count($this->currentUser->getRoles()) > 1,
      '#items_content' => $this->menuOfTypeNodes(),
      '#items_medias' => $this->menuOfMedias(),
      '#items_others' => $this->menuOfOthers(),
      '#attached' => [
        'library' => ['content_dashboard/dashboard'],
      ],
    ];
  }

  /**
   * Get menu of node types.
   *
   * @return array
   *   A list of node types.
   */
  private function menuOfTypeNodes() {
    $items_content = [];
    $node_types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();

    // Sort by name.
    uasort($node_types, function ($a, $b) {
      return strnatcasecmp($a->label(), $b->label());
    });

    foreach ($node_types as $node_type) {
      $access_handler = $this->entityTypeManager->getAccessControlHandler('node');
      $access = $access_handler->createAccess($node_type->id());

      if (!$access) {
        continue;
      }

      $items_content[] = [
        'title' => $node_type->label(),
        'description' => $node_type->getDescription(),
        'url' => Url::fromRoute('view.content.page_1', ['type' => $node_type->id()])->toString(),
        'type' => $node_type->id(),
      ];
    }

    return $items_content;
  }

  /**
   * Get menu of different media types.
   *
   * @return array
   *   A list of media types.
   */
  private function menuOfMedias() {
    $medias = [];

    if (!$this->moduleHandler()->moduleExists('media') || !$this->currentUser->hasPermission('access media overview')) {
      return $medias;
    }

    // Load all media types from the database.
    $media_types = $this->entityTypeManager->getStorage('media_type')->loadMultiple();

    // Loop through each media type and add to the list of medias.
    foreach ($media_types as $media_type) {
      $label = $media_type->label();
      $machine_name = $media_type->id();

      $medias[] = [
        'title' => $this->t('@label', ['@label' => $label]),
        'description' => $this->t('List of @type media items.', ['@type' => strtolower($label)]),
        'url' => Url::fromRoute('entity.media.collection', [], ['query' => ['type' => $machine_name]])->toString(),
        'add_url' => Url::fromRoute('entity.media.add_form', ['media_type' => $machine_name])->toString(),
      ];
    }

    return $medias;
  }

  /**
   * Get menu of other forms, taxonomies.
   *
   * @return array
   *   A list of other menu items.
   */
  private function menuOfOthers() {
    $others = [];

    if ($this->currentUser->hasPermission('access user profiles')) {
      $others[] = [
        'title' => $this->t('Users'),
        'description' => $this->t('User list'),
        'url' => Url::fromRoute('view.user_admin_people.page_1')->toString(),
      ];
    }

    if ($this->moduleHandler()->moduleExists('webform') && $this->currentUser->hasPermission('access webform overview')) {
      $others[] = [
        'title' => $this->t('Forms'),
        'description' => $this->t('List of forms and submissions'),
        'url' => Url::fromRoute('entity.webform.collection')->toString(),
      ];
    }

    if ($this->moduleHandler()->moduleExists('taxonomy') && $this->currentUser->hasPermission('access taxonomy overview')) {
      $others[] = [
        'title' => $this->t('Taxonomies'),
        'description' => $this->t('List of taxonomies'),
        'url' => Url::fromRoute('entity.taxonomy_vocabulary.collection')->toString(),
      ];
    }

    if ($this->currentUser->hasPermission('administer site configuration')) {
      $others[] = [
        'title' => $this->t('Basic site settings'),
        'description' => $this->t('Configuration the basic site settings'),
        'url' => Url::fromRoute('system.site_information_settings')->toString(),
      ];
    }

    return $others;
  }

}
